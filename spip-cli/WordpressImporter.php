<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class WordpressImporter extends Command {
	public $input = null;
	public $output = null;
	public $dir_wordpress = null;
	public $wp_version = null;
	public $base = 'wordpress';
	public $update = false;
	
	protected function configure() {
		$this
			->setName('wordpress:importer')
			->setDescription('Importe un site Wordpress dans un site SPIP')
			->setHelp('Pour lancer la commande, vous devez avoir préalablement ajouté la base de données du Wordpress en tant que base externe dans votre SPIP, et fournir en argument le dossier où se trouve les fichiers du Wordpress.

Lorsqu’un contenu est déjà importé (auteur, article, etc), une trace est gardé et il ne sera jamais réimporté. Vous pouvez donc lancer la commande plusieurs fois sans soucis, seul les nouveaux contenus jamais importés seront migrés. Cela permet notamment de continuer à lancer la commande si le site Wordpress continue d’évoluer.')
			->addArgument(
				'dir_wordpress',
				InputArgument::REQUIRED,
				'Chemin vers le dossier d’installation du Wordpress'
			)
			->addOption(
				'base',
				'b',
				InputOption::VALUE_OPTIONAL,
				'Identifiant de la base Wordpress déclarée dans SPIP',
				'wordpress'
			)
			->addOption(
				'traitements',
				't',
				InputOption::VALUE_OPTIONAL,
				'Liste de traitements séparés par des virgules, si on veut n’en lancer que certains.'
			)
			->addOption(
				'info',
				'i',
				InputOption::VALUE_OPTIONAL,
				'Affiche la version du Wordpress et les traitements disponibles.'
			)
			->addOption(
				'update',
				'u',
				InputOption::VALUE_NONE,
				'Met à jour les contenus déjà migrés avec la version du Wordpress. Peut être utile si le site Wordpress continue d’évoluer.'
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		global $spip_racine;
		global $spip_loaded;
		
		// Facilité
		$this->input = $input;
		$this->output = $output;
		
		// Si on est bien dans un dossier SPIP
		if ($spip_loaded) {
			// Dossier sur le disque où se trouve les fichiers du Wordpress
			$this->dir_wordpress = rtrim($input->getArgument('dir_wordpress'), '/') . '/';
			
			// Identifiant de la base Wordpress dans SPIP
			$this->base = $input->getOption('base');
			
			// On va chercher la version de Wordpress dont il s'agit
			include_once $this->dir_wordpress . 'wp-includes/version.php';
			$this->wp_version = $wp_version;
			
			// Est-ce qu'on doit mettre à jourles choses déjà migrées ?
			$this->update = $input->getOption('update');
			
			$traitements_disponibles = array(
				'importer_metas',
				'importer_auteurs',
				'importer_rubriques',
				'importer_mots',
				'importer_documents',
				'importer_articles',
				'importer_commentaires',
			);
			$traitements_disponibles = pipeline('w2spip_traitements', $traitements_disponibles);
			
			// Infos
			$output->writeln(array(
				'<info>C’est parti pour importer ce Wordpress !</info>',
				'* <comment>'. ($this->update ? 'Les contenus déjà migrés seront mis à jour.' : 'Les contenus déjà migrés ne seront pas ré-importés.') .'</comment>',
				'* <comment>Version</comment> : ' . $this->wp_version,
				'* <comment>Base</comment> : ' . $this->base,
				'* <comment>Fichiers</comment> : ' . $this->dir_wordpress,
				'* <comment>Traitements disponibles</comment> : ' . join(', ', $traitements_disponibles),
				'',
			));
			
			// Si on cherche juste à lire les infos, on s'arrête là
			if ($input->hasParameterOption(array('--info', '-i'))) {
				exit;
			}
			
			// Peut-être qu'on veut lancer seulement certains traitements
			if ($traitements_ok = $input->getOption('traitements')) {
				$traitements_ok = array_map('trim', explode(',', $traitements_ok));
				$traitements_ok = array_intersect($traitements_disponibles, $traitements_ok);
			}
			else {
				$traitements_ok = $traitements_disponibles;
			}
			
			foreach ($traitements_ok as $traitement) {
				$this->appliquer_traitement($traitement);
			}
		}
		else{
			$output->writeln('<error>Vous devez lancer la commande depuis un site SPIP pour importer le contenu Wordpress.</error>');
		}
	}
	
	protected function appliquer_traitement($traitement) {
		$decoupe_version = explode('.', $this->wp_version);
		$version_X = $decoupe_version[0];
		$version_Y = $decoupe_version[1];
		
		$fonction = '';
		// S'il existe une fonction wp2spip_$fonction_X_Y
		if ($f = charger_fonction("{$version_X}_{$version_Y}", "wp2spip/$traitement", true)) {
			$fonction = $f;
		}
		// S'il existe une fonction wp2spip_$fonction_X
		elseif ($f = charger_fonction("$version_X", "wp2spip/$traitement", true)) {
			$fonction = $f;
		}
		// Sinon une fonction générique wp2spip_$fonction qui vaudrait pour toutes les versions
		elseif ($f = charger_fonction("$traitement", "wp2spip", true)) {
			$fonction = $f;
		}
		// Sinon rien, on peut pas faire cette opération
		else {
			$this->output->writeln("\n<error>Aucune fonction implémentée pour le traitement « $traitement » pour cette version {$this->wp_version}.</error>");
		}
		
		// On lance le traitement trouvé
		if ($fonction) {
			$this->output->writeln("\n<info>Lancement du traitement « $traitement »…</info>");
			$fonction($this);
		}
	}
}
